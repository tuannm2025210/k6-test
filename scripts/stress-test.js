import http from 'k6/http'
import {check, sleep} from 'k6'
import {Counter} from 'k6/metrics'

// ------------------------------------ helper
export function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        let r = (Math.random() * 16) | 0,
            v = c === 'x' ? r : (r & 0x3) | 0x8
        return v.toString(16)
    })
}

export function randomIntBetween(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
}

export function randomItem(arrayOfItems) {
    return arrayOfItems[Math.floor(Math.random() * arrayOfItems.length)]
}

export function randomString(
    length,
    charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
) {
    let res = ''
    while (length--) res += charset[(Math.random() * charset.length) | 0]
    return res
}

// ------------------------------------ mock data

const mockupInfos = [
    // {
    //     name: 'Front_Model',
    //     side: 'front',
    //     type: 'warp-new-standard',
    //     parts: [
    //         {
    //             name: 'Front_Model.Background',
    //             image_path:
    //                 'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Model.Background.png'
    //         },
    //         {
    //             name: 'Front_Model.Front.Front',
    //             image_path:
    //                 'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Model.Front.Front.png'
    //         },
    //         {
    //             name: 'Front_Model.Front.Colors',
    //             image_path:
    //                 'https://platform126.s3.us-west-1.amazonaws.com/product-mockups/Transparent.png',
    //             is_color_image: true,
    //             '#1a223b':
    //                 'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Model.Front.Colors.Navy.png',
    //             '#ffffff':
    //                 'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Model.Front.Colors.White.png',
    //             '#000000':
    //                 'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Model.Front.Colors.Black.png',
    //             '#f2f2f2':
    //                 'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Model.Front.Colors.Sport%2520Grey.png',
    //             '#3f403f':
    //                 'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Model.Front.Colors.Dark%2520Heather.png'
    //         },
    //         {
    //             name: 'Front_Model.Front.Design.Design',
    //             side: 'front',
    //             mask_path:
    //                 'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Model.Front.Design.Design.png',
    //             warp_type: 'warp_npy',
    //             warp_info: {
    //                 model: 'https://platform126.s3.us-west-1.amazonaws.com/mockups/platform126-dev/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/model/Front_Model.Front.Design.Design.model.npy',
    //                 artwork_width: 2000,
    //                 artwork_height: 2000
    //             }
    //         },
    //         {
    //             name: 'Front_Model.Front.fx.multiply',
    //             image_path:
    //                 'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Model.Front.fx.multiply.png',
    //             blend_mode: 'multiply'
    //         },
    //         {
    //             name: 'Front_Model.Front.fx.screen',
    //             image_path:
    //                 'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Model.Front.fx.screen.png',
    //             blend_mode: 'screen'
    //         },
    //         {
    //             name: 'Front_Model.Front.fx.Thread',
    //             image_path:
    //                 'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Model.Front.fx.Thread.png'
    //         }
    //     ],
    //     size: {
    //         width: 1000,
    //         height: 1000
    //     }
    // },
    // {
    //     name: 'Back_Model',
    //     side: 'back',
    //     type: 'warp-new-standard',
    //     parts: [
    //         {
    //             name: 'Back_Model.Background',
    //             image_path:
    //                 'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Back_Model.Background.png'
    //         },
    //         {
    //             name: 'Back_Model.Back.Back',
    //             image_path:
    //                 'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Back_Model.Back.Back.png'
    //         },
    //         {
    //             name: 'Back_Model.Back.Colors',
    //             image_path:
    //                 'https://platform126.s3.us-west-1.amazonaws.com/product-mockups/Transparent.png',
    //             is_color_image: true,
    //             '#1a223b':
    //                 'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Back_Model.Back.Colors.Navy.png',
    //             '#ffffff':
    //                 'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Back_Model.Back.Colors.White.png',
    //             '#000000':
    //                 'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Back_Model.Back.Colors.Black.png',
    //             '#f2f2f2':
    //                 'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Back_Model.Back.Colors.Sport%2520Grey.png',
    //             '#3f403f':
    //                 'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Back_Model.Back.Colors.Dark%2520Heather.png'
    //         },
    //         {
    //             name: 'Back_Model.Back.Design.Design',
    //             side: 'back',
    //             mask_path:
    //                 'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Back_Model.Back.Design.Design.png',
    //             warp_type: 'warp_npy',
    //             warp_info: {
    //                 model: 'https://platform126.s3.us-west-1.amazonaws.com/mockups/platform126-dev/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/model/Back_Model.Back.Design.Design.model.npy',
    //                 artwork_width: 2000,
    //                 artwork_height: 2000
    //             }
    //         },
    //         {
    //             name: 'Back_Model.Back.fx.multiply',
    //             image_path:
    //                 'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Back_Model.Back.fx.multiply.png',
    //             blend_mode: 'multiply'
    //         },
    //         {
    //             name: 'Back_Model.Back.fx.screen',
    //             image_path:
    //                 'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Back_Model.Back.fx.screen.png',
    //             blend_mode: 'screen'
    //         }
    //     ],
    //     size: {
    //         width: 1000,
    //         height: 1000
    //     }
    // },
    {
        name: 'Front_Back_Model',
        side: 'back',
        type: 'warp-new-standard',
        parts: [
            {
                name: 'Front_Back_Model.Background',
                image_path:
                    'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Back_Model.Background.png'
            },
            {
                name: 'Front_Back_Model.Back.Back',
                image_path:
                    'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Back_Model.Back.Back.png'
            },
            {
                name: 'Front_Back_Model.Back.Colors',
                image_path:
                    'https://platform126.s3.us-west-1.amazonaws.com/product-mockups/Transparent.png',
                is_color_image: true,
                '#1a223b':
                    'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Back_Model.Back.Colors.Navy.png',
                '#ffffff':
                    'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Back_Model.Back.Colors.White.png',
                '#000000':
                    'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Back_Model.Back.Colors.Black.png',
                '#f2f2f2':
                    'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Back_Model.Back.Colors.Sport%2520Grey.png',
                '#3f403f':
                    'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Back_Model.Back.Colors.Dark%2520Heather.png'
            },
            {
                name: 'Front_Back_Model.Back.Design.Design',
                side: 'back',
                mask_path:
                    'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Back_Model.Back.Design.Design.png',
                warp_type: 'warp_npy',
                warp_info: {
                    model: 'https://platform126.s3.us-west-1.amazonaws.com/mockups/platform126-dev/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/model/Front_Back_Model.Back.Design.Design.model.npy',
                    artwork_width: 2000,
                    artwork_height: 2000
                }
            },
            {
                name: 'Front_Back_Model.Back.fx.multiply',
                image_path:
                    'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Back_Model.Back.fx.multiply.png',
                blend_mode: 'multiply'
            },
            {
                name: 'Front_Back_Model.Back.fx.screen',
                image_path:
                    'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Back_Model.Back.fx.screen.png',
                blend_mode: 'screen'
            },
            {
                name: 'Front_Back_Model.Front.Front',
                image_path:
                    'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Back_Model.Front.Front.png'
            },
            {
                name: 'Front_Back_Model.Front.Colors',
                image_path:
                    'https://platform126.s3.us-west-1.amazonaws.com/product-mockups/Transparent.png',
                is_color_image: true,
                '#1a223b':
                    'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Back_Model.Front.Colors.Navy.png',
                '#ffffff':
                    'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Back_Model.Front.Colors.White.png',
                '#000000':
                    'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Back_Model.Front.Colors.Black.png',
                '#f2f2f2':
                    'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Back_Model.Front.Colors.Sport%2520Grey.png',
                '#3f403f':
                    'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Back_Model.Front.Colors.Dark%2520Heather.png'
            },
            {
                name: 'Front_Back_Model.Front.Design.Design',
                side: 'front',
                mask_path:
                    'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Back_Model.Front.Design.Design.png',
                warp_type: 'warp_npy',
                warp_info: {
                    model: 'https://platform126.s3.us-west-1.amazonaws.com/mockups/platform126-dev/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/model/Front_Back_Model.Front.Design.Design.model.npy',
                    artwork_width: 2000,
                    artwork_height: 2000
                }
            },
            {
                name: 'Front_Back_Model.Front.fx.multiply',
                image_path:
                    'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Back_Model.Front.fx.multiply.png',
                blend_mode: 'multiply'
            },
            {
                name: 'Front_Back_Model.Front.fx.screen',
                image_path:
                    'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Back_Model.Front.fx.screen.png',
                blend_mode: 'screen'
            },
            {
                name: 'Front_Back_Model.Front.fx.Thread',
                image_path:
                    'https://platform126.s3.us-west-1.amazonaws.com/mockups/product-mockups/classic-unisex-hoodie-made-in-us/20-02-23/image-mask/Front_Back_Model.Front.fx.Thread.png'
            }
        ],
        size: {
            width: 1000,
            height: 1000
        }
    }
]

const getBody = (name, mockupInfos, url) => ({
    bucketImage: 'ss42nizi',
    productId: '63f3207e40bc94bbbd9d0f0c',
    productType: 'hoodie_db',
    color: '#000000',
    productName: name,
    artworkUrls: {
        front: url.front,
        back: url.back
    },
    mockupInfo: {
        mockup_infos: mockupInfos,
        consistency_name: 'classic-unisex-hoodie-made-in-us',
        artwork_side_info: 'artwork-per-side',
        prefix_name: 'classic-unisex-hoodie-made-in-us',
        version: 'v2',
        use_origin_artwork: true
    }
})

// ------------------------------------ test
// custom metrics
const totalSuccess = new Counter('total_success')
const totalErr = new Counter('total_error')

export const options = {
    scenarios: {
        constant_request_rate: {
            executor: 'constant-arrival-rate',
            rate: 100,
            timeUnit: '1m',
            duration: '10m',
            preAllocatedVUs: 2,
            maxVUs: 20
        }
    }
    // thresholds: {
    //     http_req_duration: ['avg<100', 'p(95)<200']
    // },
    // stages: [
    //     // {vus: 2, duration: '60s', target: 40},
    //     // {vus: 2, duration: '60s', target: 30},
    //     {vus: 2, duration: '60s', target: 40}
    // ],
}

export default function () {
    const initArr = [
        'https://s3-temp-behaviour-prod.s3-accelerate.amazonaws.com/l505a68d/temps/ce1e59b051854b3b9dc58ac1559cd692-private',
        'https://s3-temp-behaviour-prod.s3-accelerate.amazonaws.com/l505a68d/temps/21b02e229abe46778ed808be2dde1ccb-private',
        'https://s3-temp-behaviour-prod.s3-accelerate.amazonaws.com/l505a68d/temps/cb80816bce0e488888489383924a9b68-private'
    ]

    const randomUrl = () => {
        const front = `${randomItem(initArr)}?${randomString(8)}`
        const back = `${randomItem(initArr)}?${randomString(8)}`
        return {back, front}
    }

    const name = `Perf Test product ${randomString(10)}`
    // const randomMockupInfo = [randomItem(mockupInfos)]
    const body = getBody(name, mockupInfos, randomUrl())

    if (body) {
        let resp
        try {
            resp = http.post(
                'https://mockup-v2-staging.merchize.com/mockup-gen/from-configs?maxWorkers=2&resourceCheck=true',
                JSON.stringify(body),
                {
                    headers: {'Content-Type': 'application/json'},
                    timeout: '3m'
                }
            )
            if (resp.status !== 200) {
                const meta = {
                    label: 'error',
                    info: JSON.stringify({
                        req: resp.url,
                        body: resp.body || {},
                        res: resp.json() || resp.html(),
                        status: {
                            code: resp.status,
                            text: resp.status_text
                        }
                    })
                }
                totalErr.add(1, meta)
            } else {
                const {success, data, message} = resp.json()
                if (success) {
                    const meta = {
                        label: 'success',
                        info: JSON.stringify({
                            req: resp.url,
                            payload: body,
                            body: resp.body || {},
                            mockup_links: data.map((item) => item.location)
                        })
                    }
                    totalSuccess.add(1, meta)
                } else {
                    const meta = {
                        label: 'error',
                        info: JSON.stringify({
                            req: resp.url,
                            payload: body,
                            body: resp.body,
                            message: message || ''
                        })
                    }
                    totalErr.add(1, meta)
                }
            }
        } catch (error) {
            const meta = {
                label: 'error',
                info: JSON.stringify({
                    req: resp.url,
                    payload: body,
                    body: resp.body,
                    err: error,
                    status: {
                        code: resp.status,
                        text: resp.status_text
                    }
                })
            }
            totalErr.add(1, meta)
        }
        sleep(1)
    }
}
