import http from 'k6/http'
import {sleep} from 'k6'

export const options = {
    scenarios: {
        constant_request_rate: {
            executor: 'constant-arrival-rate',
            rate: 200,
            timeUnit: '1m',
            duration: '2m',
            preAllocatedVUs: 2,
            maxVUs: 15
        }
    }
    // thresholds: {
    //     http_req_duration: ['avg<100', 'p(95)<200']
    // },
    // stages: [
    //     // {vus: 2, duration: '60s', target: 40},
    //     // {vus: 2, duration: '60s', target: 30},
    //     {vus: 2, duration: '60s', target: 40}
    // ],
}
export default function () {
    http.get('https://normal-staging.merchize.store/shop')
    http.get('https://normal-staging.merchize.store')
    sleep(1)
}
